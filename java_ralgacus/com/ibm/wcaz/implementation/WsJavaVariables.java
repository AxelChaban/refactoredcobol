package com.ibm.wcaz.implementation;

import com.ibm.jzos.fields.CobolDatatypeFactory;
import com.ibm.jzos.fields.StringField;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class WsJavaVariables implements Comparable<WsJavaVariables> {
    private String java = "JAVA";
    private String lgacjv01 = "LGACJV01";
    private String wsStatus = "";
    
    public WsJavaVariables() {}
    
    public WsJavaVariables(String java, String lgacjv01, String wsStatus) {
        this.java = java;
        this.lgacjv01 = lgacjv01;
        this.wsStatus = wsStatus;
    }
    
    public WsJavaVariables(WsJavaVariables that) {
        this.java = that.java;
        this.lgacjv01 = that.lgacjv01;
        this.wsStatus = that.wsStatus;
    }
    
    protected WsJavaVariables(byte[] bytes, int offset) {
        setBytes(bytes, offset);
    }
    
    protected WsJavaVariables(byte[] bytes) {
        this(bytes, 0);
    }
    
    public static WsJavaVariables fromBytes(byte[] bytes, int offset) {
        return new WsJavaVariables(bytes, offset);
    }
    
    public static WsJavaVariables fromBytes(byte[] bytes) {
        return fromBytes(bytes, 0);
    }
    
    public static WsJavaVariables fromBytes(String bytes) {
        try {
            return fromBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public String getJava() {
        return this.java;
    }
    
    public void setJava(String java) {
        this.java = java;
    }
    
    public String getLgacjv01() {
        return this.lgacjv01;
    }
    
    public void setLgacjv01(String lgacjv01) {
        this.lgacjv01 = lgacjv01;
    }
    
    public String getWsStatus() {
        return this.wsStatus;
    }
    
    public void setWsStatus(String wsStatus) {
        this.wsStatus = wsStatus;
    }
    public void reset() {
        java = "";
        lgacjv01 = "";
        wsStatus = "";
    }
    
    
    public void mainlineFraudCheck() {}
    
    public void mainlineInsertCustomer() {}
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("{ java=\"");
        s.append(getJava());
        s.append("\"");
        s.append(", lgacjv01=\"");
        s.append(getLgacjv01());
        s.append("\"");
        s.append(", wsStatus=\"");
        s.append(getWsStatus());
        s.append("\"");
        s.append("}");
        return s.toString();
    }
    
    public boolean equals(WsJavaVariables that) {
        return this.java.equals(that.java) &&
            this.lgacjv01.equals(that.lgacjv01) &&
            this.wsStatus.equals(that.wsStatus);
    }
    
    @Override
    public boolean equals(Object that) {
        return (that instanceof WsJavaVariables) && this.equals((WsJavaVariables)that);
    }
    
    @Override
    public int hashCode() {
        return java.hashCode() ^
            Integer.rotateLeft(lgacjv01.hashCode(), 1) ^
            Integer.rotateLeft(wsStatus.hashCode(), 2);
    }
    
    @Override
    public int compareTo(WsJavaVariables that) {
        int c = 0;
        c = this.java.compareTo(that.java);
        if ( c != 0 ) return c;
        c = this.lgacjv01.compareTo(that.lgacjv01);
        if ( c != 0 ) return c;
        c = this.wsStatus.compareTo(that.wsStatus);
        return c;
    }
    
    // Start of COBOL-compatible binary serialization metadata
    private static CobolDatatypeFactory factory = new CobolDatatypeFactory();
    static {
        factory.setStringTrimDefault(true);
        factory.setStringEncoding("IBM-1047");
    }
    
    private static final StringField JAVA = factory.getStringField(8);
    private static final StringField LGACJV_01 = factory.getStringField(8);
    private static final StringField WS_STATUS = factory.getStringField(5);
    public static final int SIZE = factory.getOffset();
    // End of COBOL-compatible binary serialization metadata
    
    public byte[] getBytes(byte[] bytes, int offset) {
        JAVA.putString(java, bytes, offset);
        LGACJV_01.putString(lgacjv01, bytes, offset);
        WS_STATUS.putString(wsStatus, bytes, offset);
        return bytes;
    }
    
    public final byte[] getBytes(byte[] bytes) {
        return getBytes(bytes, 0);
    }
    
    public final byte[] getBytes() {
        return getBytes(new byte[numBytes()]);
    }
    
    public final String toByteString() {
        try {
            return new String(getBytes(), factory.getStringEncoding());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void setBytes(byte[] bytes, int offset) {
        if (bytes.length < SIZE + offset) {
            byte[] newBytes = Arrays.copyOf(bytes, SIZE + offset);
            Arrays.fill(newBytes, bytes.length, SIZE + offset, (byte)0x40 /*default EBCDIC space character*/);
            bytes = newBytes;
        }
        java = JAVA.getString(bytes, offset);
        lgacjv01 = LGACJV_01.getString(bytes, offset);
        wsStatus = WS_STATUS.getString(bytes, offset);
    }
    
    
    public final void setBytes(byte[] bytes) {
        setBytes(bytes, 0);
    }
    
    public final void setBytes(String bytes) {
        try {
            setBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public int numBytes() {
        return SIZE;
    }
    
}
