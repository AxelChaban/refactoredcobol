package com.ibm.wcaz.implementation;

import com.ibm.cics.server.CommAreaHolder;
import com.ibm.cics.server.Task;
import com.ibm.jzos.fields.CobolDatatypeFactory;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

public class Ralgacus implements Comparable<Ralgacus> {
    public Ralgacus() {}
    
    public Ralgacus(Ralgacus that) {
    }
    
    protected Ralgacus(byte[] bytes, int offset) {
        setBytes(bytes, offset);
    }
    
    protected Ralgacus(byte[] bytes) {
        this(bytes, 0);
    }
    
    public static Ralgacus fromBytes(byte[] bytes, int offset) {
        return new Ralgacus(bytes, offset);
    }
    
    public static Ralgacus fromBytes(byte[] bytes) {
        return fromBytes(bytes, 0);
    }
    
    public static Ralgacus fromBytes(String bytes) {
        try {
            return fromBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    
    public static void mainlineCheckFirstCobol() {
    WsResponse wsResponse = new WsResponse();
    wsResponse.setWsResponseCode(lgacjv01.getWsRespCode());
    wsResponse.setWsResponseMessage(lgacjv01.getWsRespMsg());
}

    
    public static void mainlineFirstSentences(){
    Dfhcommarea1 dfhcommarea1 = new Dfhcommarea1();
    ErrorMsg errorMsg = new ErrorMsg();
    WsCommareaLengths wsCommareaLengths = new WsCommareaLengths();
    int wsCustomerLen = 72;
    WsHeader wsHeader = new WsHeader();
    WsJavaVariables wsJavaVariables = new WsJavaVariables();
    WsResponse wsResponse = new WsResponse();
    wsHeader.setWsTransid(Task.getTask().getTransactionName());
    wsHeader.setWsTermid(Task.getTask().getInvokingProgramName());
    wsHeader.setWsTasknum(Task.getTask().getTaskNumber());
}

public static void mainlineCheckFirst(){
    Dfhcommarea1 dfhcommarea1 = new Dfhcommarea1();
    ErrorMsg errorMsg = new ErrorMsg();
    WsCommareaLengths wsCommareaLengths = new WsCommareaLengths();
    int wsCustomerLen = 72;
    WsHeader wsHeader = new WsHeader();
    WsJavaVariables wsJavaVariables = new WsJavaVariables();
    WsResponse wsResponse = new WsResponse();
    wsResponse.setWsResponseCode(Integer.parseInt("00"));
    wsResponse.setWsResponseMessage("Customer record inserted successfully");
}

public static void mainlineFraudCheck(){
    Dfhcommarea1 dfhcommarea1 = new Dfhcommarea1();
    ErrorMsg errorMsg = new ErrorMsg();
    WsCommareaLengths wsCommareaLengths = new WsCommareaLengths();
    int wsCustomerLen = 72;
    WsHeader wsHeader = new WsHeader();
    WsJavaVariables wsJavaVariables = new WsJavaVariables();
    WsResponse wsResponse = new WsResponse();
    wsJavaVariables.setWsStatus("valid");
}

public static void mainlineInsertCustomer(){
    Dfhcommarea1 dfhcommarea1 = new Dfhcommarea1();
    ErrorMsg errorMsg = new ErrorMsg();
    WsCommareaLengths wsCommareaLengths = new WsCommareaLengths();
    int wsCustomerLen = 72;
    WsHeader wsHeader = new WsHeader();
    WsJavaVariables wsJavaVariables = new WsJavaVariables();
    WsResponse wsResponse = new WsResponse();
    if (dfhcommarea1.getCaNumPolicies() > 0) {
        wsResponse.setWsResponseCode(Integer.parseInt("00"));
        wsResponse.setWsResponseMessage("Customer record inserted successfully");
    }
    else {
        wsResponse.setWsResponseCode(Integer.parseInt("99"));
        wsResponse.setWsResponseMessage("Customer record not inserted");
    }
}

public static void mainlineWriteErrorMessage(){
    Dfhcommarea1 dfhcommarea1 = new Dfhcommarea1();
    ErrorMsg errorMsg = new ErrorMsg();
    WsCommareaLengths wsCommareaLengths = new WsCommareaLengths();
    int wsCustomerLen = 72;
    WsHeader wsHeader = new WsHeader();
    WsJavaVariables wsJavaVariables = new WsJavaVariables();
    WsResponse wsResponse = new WsResponse();
    errorMsg.setEmVariable(" NO COMMAREA RECEIVED");
    errorMsg.mainlineWriteErrorMessage();
}

public static void main(CommAreaHolder commArea){
    Dfhcommarea1 dfhcommarea1 = new Dfhcommarea1();
    ErrorMsg errorMsg = new ErrorMsg();
    WsCommareaLengths wsCommareaLengths = new WsCommareaLengths();
    int wsCustomerLen = 72;
    WsHeader wsHeader = new WsHeader();
    WsJavaVariables wsJavaVariables = new WsJavaVariables();
    WsResponse wsResponse = new WsResponse();
    mainlineFirstSentences();
    if (commArea.getValue().length == 0) {
        mainlineWriteErrorMessage();
        Task.getTask().abend("LGCA");
    }
    else {
        dfhcommarea1.setBytes(commArea.getValue());
        wsCommareaLengths.setWsCaHeaderLen(Integer.parseInt("12"));
        wsCommareaLengths.setWsRequiredCaLen(wsCommareaLengths.getWsCaHeaderLen() + wsCustomerLen);
        if (commArea.getValue().length < wsCommareaLengths.getWsRequiredCaLen()) {
            dfhcommarea1.setCaReturnCode(Integer.parseInt("98"));
            Task.getTask().returnToCics();
        }
        else {
            mainlineCheckFirst();
            if (wsResponse.getWsResponseCode() > 0) {
                dfhcommarea1.setCaReturnCode(wsResponse.getWsResponseCode());
                Task.getTask().returnToCics();
            }
            mainlineFraudCheck();
            if (wsJavaVariables.getWsStatus() != "valid") {
                dfhcommarea1.setCaReturnCode(Integer.parseInt("81"));
                Task.getTask().returnToCics();
            }
            mainlineInsertCustomer();
            if (dfhcommarea1.getCaReturnCode() > 0) {
                Task.getTask().returnToCics();
            }
        }
    }
}

    
    public static void main(String[] args) {
        mainlineFirstSentences();
    }
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("}");
        return s.toString();
    }
    
    public boolean equals(Ralgacus that) {
        return true;
    }
    
    @Override
    public boolean equals(Object that) {
        return (that instanceof Ralgacus) && this.equals((Ralgacus)that);
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    @Override
    public int compareTo(Ralgacus that) {
        int c = 0;
        return c;
    }
    
    // Start of COBOL-compatible binary serialization metadata
    private static CobolDatatypeFactory factory = new CobolDatatypeFactory();
    static {
        factory.setStringTrimDefault(true);
        factory.setStringEncoding("IBM-1047");
    }
    
    public static final int SIZE = factory.getOffset();
    // End of COBOL-compatible binary serialization metadata
    
    public byte[] getBytes(byte[] bytes, int offset) {
        return bytes;
    }
    
    public final byte[] getBytes(byte[] bytes) {
        return getBytes(bytes, 0);
    }
    
    public final byte[] getBytes() {
        return getBytes(new byte[numBytes()]);
    }
    
    public final String toByteString() {
        try {
            return new String(getBytes(), factory.getStringEncoding());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void setBytes(byte[] bytes, int offset) {
        if (bytes.length < SIZE + offset) {
            byte[] newBytes = Arrays.copyOf(bytes, SIZE + offset);
            Arrays.fill(newBytes, bytes.length, SIZE + offset, (byte)0x40 /*default EBCDIC space character*/);
            bytes = newBytes;
        }
    }
    
    
    public final void setBytes(byte[] bytes) {
        setBytes(bytes, 0);
    }
    
    public final void setBytes(String bytes) {
        try {
            setBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public int numBytes() {
        return SIZE;
    }
    
}
