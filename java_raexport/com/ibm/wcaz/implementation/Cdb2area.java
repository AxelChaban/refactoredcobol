package com.ibm.wcaz.implementation;

import com.ibm.jzos.fields.CobolDatatypeFactory;
import com.ibm.jzos.fields.ExternalDecimalAsIntField;
import com.ibm.jzos.fields.ExternalDecimalAsLongField;
import com.ibm.jzos.fields.StringField;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class Cdb2area implements Comparable<Cdb2area> {
    private String d2RequestId = "";
    private int d2ReturnCode;
    private long d2CustomerNum;
    private String d2CustsecrPass = "";
    private String d2CustsecrCount = "";
    private char d2CustsecrState = ' ';
    private String d2CustsecrData = "";
    
    public Cdb2area() {}
    
    public Cdb2area(String d2RequestId, int d2ReturnCode, long d2CustomerNum, String d2CustsecrPass, String d2CustsecrCount, char d2CustsecrState, String d2CustsecrData) {
        this.d2RequestId = d2RequestId;
        this.d2ReturnCode = d2ReturnCode;
        this.d2CustomerNum = d2CustomerNum;
        this.d2CustsecrPass = d2CustsecrPass;
        this.d2CustsecrCount = d2CustsecrCount;
        this.d2CustsecrState = d2CustsecrState;
        this.d2CustsecrData = d2CustsecrData;
    }
    
    public Cdb2area(Cdb2area that) {
        this.d2RequestId = that.d2RequestId;
        this.d2ReturnCode = that.d2ReturnCode;
        this.d2CustomerNum = that.d2CustomerNum;
        this.d2CustsecrPass = that.d2CustsecrPass;
        this.d2CustsecrCount = that.d2CustsecrCount;
        this.d2CustsecrState = that.d2CustsecrState;
        this.d2CustsecrData = that.d2CustsecrData;
    }
    
    protected Cdb2area(byte[] bytes, int offset) {
        setBytes(bytes, offset);
    }
    
    protected Cdb2area(byte[] bytes) {
        this(bytes, 0);
    }
    
    public static Cdb2area fromBytes(byte[] bytes, int offset) {
        return new Cdb2area(bytes, offset);
    }
    
    public static Cdb2area fromBytes(byte[] bytes) {
        return fromBytes(bytes, 0);
    }
    
    public static Cdb2area fromBytes(String bytes) {
        try {
            return fromBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public String getD2RequestId() {
        return this.d2RequestId;
    }
    
    public void setD2RequestId(String d2RequestId) {
        this.d2RequestId = d2RequestId;
    }
    
    public int getD2ReturnCode() {
        return this.d2ReturnCode;
    }
    
    public void setD2ReturnCode(int d2ReturnCode) {
        this.d2ReturnCode = d2ReturnCode;
    }
    
    public long getD2CustomerNum() {
        return this.d2CustomerNum;
    }
    
    public void setD2CustomerNum(long d2CustomerNum) {
        this.d2CustomerNum = d2CustomerNum;
    }
    
    public String getD2CustsecrPass() {
        return this.d2CustsecrPass;
    }
    
    public void setD2CustsecrPass(String d2CustsecrPass) {
        this.d2CustsecrPass = d2CustsecrPass;
    }
    
    public String getD2CustsecrCount() {
        return this.d2CustsecrCount;
    }
    
    public void setD2CustsecrCount(String d2CustsecrCount) {
        this.d2CustsecrCount = d2CustsecrCount;
    }
    
    public String getD2CustsecrState() {
        return this.d2CustsecrState;
    }
    
    public void setD2CustsecrState(char d2CustsecrState) {
        this.d2CustsecrState = d2CustsecrState;
    }
    
    public String getD2CustsecrData() {
        return this.d2CustsecrData;
    }
    
    public void setD2CustsecrData(String d2CustsecrData) {
        this.d2CustsecrData = d2CustsecrData;
    }
    public void reset() {
        d2RequestId = "";
        d2ReturnCode = 0;
        d2CustomerNum = 0;
        d2CustsecrPass = "";
        d2CustsecrCount = "";
        d2CustsecrState = ' ';
        d2CustsecrData = "";
    }
    
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("{ d2RequestId=\"");
        s.append(getD2RequestId());
        s.append("\"");
        s.append(", d2ReturnCode=\"");
        s.append(getD2ReturnCode());
        s.append("\"");
        s.append(", d2CustomerNum=\"");
        s.append(getD2CustomerNum());
        s.append("\"");
        s.append(", d2CustsecrPass=\"");
        s.append(getD2CustsecrPass());
        s.append("\"");
        s.append(", d2CustsecrCount=\"");
        s.append(getD2CustsecrCount());
        s.append("\"");
        s.append(", d2CustsecrState=\"");
        s.append(getD2CustsecrState());
        s.append("\"");
        s.append(", d2CustsecrData=\"");
        s.append(getD2CustsecrData());
        s.append("\"");
        s.append("}");
        return s.toString();
    }
    
    public boolean equals(Cdb2area that) {
        return this.d2RequestId.equals(that.d2RequestId) &&
            this.d2ReturnCode == that.d2ReturnCode &&
            this.d2CustomerNum == that.d2CustomerNum &&
            this.d2CustsecrPass.equals(that.d2CustsecrPass) &&
            this.d2CustsecrCount.equals(that.d2CustsecrCount) &&
            this.d2CustsecrState == that.d2CustsecrState &&
            this.d2CustsecrData.equals(that.d2CustsecrData);
    }
    
    @Override
    public boolean equals(Object that) {
        return (that instanceof Cdb2area) && this.equals((Cdb2area)that);
    }
    
    @Override
    public int hashCode() {
        return d2RequestId.hashCode() ^
            Integer.rotateLeft(Integer.hashCode(d2ReturnCode), 1) ^
            Integer.rotateLeft(Long.hashCode(d2CustomerNum), 2) ^
            Integer.rotateLeft(d2CustsecrPass.hashCode(), 3) ^
            Integer.rotateLeft(d2CustsecrCount.hashCode(), 4) ^
            Integer.rotateLeft(Character.hashCode(d2CustsecrState), 5) ^
            Integer.rotateLeft(d2CustsecrData.hashCode(), 6);
    }
    
    @Override
    public int compareTo(Cdb2area that) {
        int c = 0;
        c = this.d2RequestId.compareTo(that.d2RequestId);
        if ( c != 0 ) return c;
        c = Integer.compare(this.d2ReturnCode, that.d2ReturnCode);
        if ( c != 0 ) return c;
        c = Long.compare(this.d2CustomerNum, that.d2CustomerNum);
        if ( c != 0 ) return c;
        c = this.d2CustsecrPass.compareTo(that.d2CustsecrPass);
        if ( c != 0 ) return c;
        c = this.d2CustsecrCount.compareTo(that.d2CustsecrCount);
        if ( c != 0 ) return c;
        c = Character.compare(this.d2CustsecrState, that.d2CustsecrState);
        if ( c != 0 ) return c;
        c = this.d2CustsecrData.compareTo(that.d2CustsecrData);
        return c;
    }
    
    // Start of COBOL-compatible binary serialization metadata
    private static CobolDatatypeFactory factory = new CobolDatatypeFactory();
    static {
        factory.setStringTrimDefault(true);
        factory.setStringEncoding("IBM-1047");
    }
    
    private static final StringField D_2_REQUEST_ID = factory.getStringField(6);
    private static final ExternalDecimalAsIntField D_2_RETURN_CODE = factory.getExternalDecimalAsIntField(2, true);
    private static final ExternalDecimalAsLongField D_2_CUSTOMER_NUM = factory.getExternalDecimalAsLongField(10, true);
    private static final StringField D_2_CUSTSECR_PASS = factory.getStringField(32);
    private static final StringField D_2_CUSTSECR_COUNT = factory.getStringField(4);
    private static final StringField D_2_CUSTSECR_STATE = factory.getStringField(1, false);
    private static final StringField D_2_CUSTSECR_DATA = factory.getStringField(32445);
    public static final int SIZE = factory.getOffset();
    // End of COBOL-compatible binary serialization metadata
    
    public byte[] getBytes(byte[] bytes, int offset) {
        D_2_REQUEST_ID.putString(d2RequestId, bytes, offset);
        D_2_RETURN_CODE.putInt(d2ReturnCode, bytes, offset);
        D_2_CUSTOMER_NUM.putLong(d2CustomerNum, bytes, offset);
        D_2_CUSTSECR_PASS.putString(d2CustsecrPass, bytes, offset);
        D_2_CUSTSECR_COUNT.putString(d2CustsecrCount, bytes, offset);
        D_2_CUSTSECR_STATE.putString(Character.toString(d2CustsecrState), bytes, offset);
        D_2_CUSTSECR_DATA.putString(d2CustsecrData, bytes, offset);
        return bytes;
    }
    
    public final byte[] getBytes(byte[] bytes) {
        return getBytes(bytes, 0);
    }
    
    public final byte[] getBytes() {
        return getBytes(new byte[numBytes()]);
    }
    
    public final String toByteString() {
        try {
            return new String(getBytes(), factory.getStringEncoding());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void setBytes(byte[] bytes, int offset) {
        if (bytes.length < SIZE + offset) {
            byte[] newBytes = Arrays.copyOf(bytes, SIZE + offset);
            Arrays.fill(newBytes, bytes.length, SIZE + offset, (byte)0x40 /*default EBCDIC space character*/);
            bytes = newBytes;
        }
        d2RequestId = D_2_REQUEST_ID.getString(bytes, offset);
        d2ReturnCode = D_2_RETURN_CODE.getInt(bytes, offset);
        d2CustomerNum = D_2_CUSTOMER_NUM.getLong(bytes, offset);
        d2CustsecrPass = D_2_CUSTSECR_PASS.getString(bytes, offset);
        d2CustsecrCount = D_2_CUSTSECR_COUNT.getString(bytes, offset);
        d2CustsecrState = D_2_CUSTSECR_STATE.getString(bytes, offset).charAt(0);
        d2CustsecrData = D_2_CUSTSECR_DATA.getString(bytes, offset);
    }
    
    
    public final void setBytes(byte[] bytes) {
        setBytes(bytes, 0);
    }
    
    public final void setBytes(String bytes) {
        try {
            setBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public int numBytes() {
        return SIZE;
    }

    public void setD2CustsecrDate(String format) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'setD2CustsecrDate'");
    }

    public String getD2CustsecrDate() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getD2CustsecrDate'");
    }
    
}
