package com.ibm.wcaz.implementation;

import com.ibm.jzos.fields.BinaryAsIntField;
import com.ibm.jzos.fields.CobolDatatypeFactory;
import com.ibm.jzos.fields.ExternalDecimalAsIntField;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class WsCommareaLengths implements Comparable<WsCommareaLengths> {
    private int wsCaHeaderLen = 18;
    private int wsRequiredCaLen = 0;
    
    public WsCommareaLengths() {}
    
    public WsCommareaLengths(int wsCaHeaderLen, int wsRequiredCaLen) {
        this.wsCaHeaderLen = wsCaHeaderLen;
        this.wsRequiredCaLen = wsRequiredCaLen;
    }
    
    public WsCommareaLengths(WsCommareaLengths that) {
        this.wsCaHeaderLen = that.wsCaHeaderLen;
        this.wsRequiredCaLen = that.wsRequiredCaLen;
    }
    
    protected WsCommareaLengths(byte[] bytes, int offset) {
        setBytes(bytes, offset);
    }
    
    protected WsCommareaLengths(byte[] bytes) {
        this(bytes, 0);
    }
    
    public static WsCommareaLengths fromBytes(byte[] bytes, int offset) {
        return new WsCommareaLengths(bytes, offset);
    }
    
    public static WsCommareaLengths fromBytes(byte[] bytes) {
        return fromBytes(bytes, 0);
    }
    
    public static WsCommareaLengths fromBytes(String bytes) {
        try {
            return fromBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public int getWsCaHeaderLen() {
        return this.wsCaHeaderLen;
    }
    
    public void setWsCaHeaderLen(int wsCaHeaderLen) {
        this.wsCaHeaderLen = wsCaHeaderLen;
    }
    
    public int getWsRequiredCaLen() {
        return this.wsRequiredCaLen;
    }
    
    public void setWsRequiredCaLen(int wsRequiredCaLen) {
        this.wsRequiredCaLen = wsRequiredCaLen;
    }
    public void reset() {
        wsCaHeaderLen = 0;
        wsRequiredCaLen = 0;
    }
    
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("{ wsCaHeaderLen=\"");
        s.append(getWsCaHeaderLen());
        s.append("\"");
        s.append(", wsRequiredCaLen=\"");
        s.append(getWsRequiredCaLen());
        s.append("\"");
        s.append("}");
        return s.toString();
    }
    
    public boolean equals(WsCommareaLengths that) {
        return this.wsCaHeaderLen == that.wsCaHeaderLen &&
            this.wsRequiredCaLen == that.wsRequiredCaLen;
    }
    
    @Override
    public boolean equals(Object that) {
        return (that instanceof WsCommareaLengths) && this.equals((WsCommareaLengths)that);
    }
    
    @Override
    public int hashCode() {
        return Integer.hashCode(wsCaHeaderLen) ^
            Integer.rotateLeft(Integer.hashCode(wsRequiredCaLen), 1);
    }
    
    @Override
    public int compareTo(WsCommareaLengths that) {
        int c = 0;
        c = Integer.compare(this.wsCaHeaderLen, that.wsCaHeaderLen);
        if ( c != 0 ) return c;
        c = Integer.compare(this.wsRequiredCaLen, that.wsRequiredCaLen);
        return c;
    }
    
    // Start of COBOL-compatible binary serialization metadata
    private static CobolDatatypeFactory factory = new CobolDatatypeFactory();
    static {
        factory.setStringTrimDefault(true);
        factory.setStringEncoding("IBM-1047");
    }
    
    private static final BinaryAsIntField WS_CA_HEADER_LEN = factory.getBinaryAsIntField(5, true);
    private static final ExternalDecimalAsIntField WS_REQUIRED_CA_LEN = factory.getExternalDecimalAsIntField(5, true);
    public static final int SIZE = factory.getOffset();
    // End of COBOL-compatible binary serialization metadata
    
    public byte[] getBytes(byte[] bytes, int offset) {
        WS_CA_HEADER_LEN.putInt(wsCaHeaderLen, bytes, offset);
        WS_REQUIRED_CA_LEN.putInt(wsRequiredCaLen, bytes, offset);
        return bytes;
    }
    
    public final byte[] getBytes(byte[] bytes) {
        return getBytes(bytes, 0);
    }
    
    public final byte[] getBytes() {
        return getBytes(new byte[numBytes()]);
    }
    
    public final String toByteString() {
        try {
            return new String(getBytes(), factory.getStringEncoding());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void setBytes(byte[] bytes, int offset) {
        if (bytes.length < SIZE + offset) {
            byte[] newBytes = Arrays.copyOf(bytes, SIZE + offset);
            Arrays.fill(newBytes, bytes.length, SIZE + offset, (byte)0x40 /*default EBCDIC space character*/);
            bytes = newBytes;
        }
        wsCaHeaderLen = WS_CA_HEADER_LEN.getInt(bytes, offset);
        wsRequiredCaLen = WS_REQUIRED_CA_LEN.getInt(bytes, offset);
    }
    
    
    public final void setBytes(byte[] bytes) {
        setBytes(bytes, 0);
    }
    
    public final void setBytes(String bytes) {
        try {
            setBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public int numBytes() {
        return SIZE;
    }
    
}
