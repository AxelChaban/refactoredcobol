package com.ibm.wcaz.implementation;

import com.ibm.cics.server.Container;
import com.ibm.cics.server.Task;
import com.ibm.jzos.fields.BinaryAsLongField;
import com.ibm.jzos.fields.CobolDatatypeFactory;
import com.ibm.jzos.fields.StringField;
import java.io.UnsupportedEncodingException;
import java.nio.channels.Channel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.sql.*;
import java.time.LocalDateTime;

public class Raexport implements Comparable<Raexport> {
    private static long db2CustomernumInt;
    private static String lgacNcs = "ON";

    public Raexport() {
    }

    public Raexport(long db2CustomernumInt, String lgacNcs) {
        this.db2CustomernumInt = db2CustomernumInt;
        this.lgacNcs = lgacNcs;
    }

    public Raexport(Raexport that) {
        this.db2CustomernumInt = that.db2CustomernumInt;
        this.lgacNcs = that.lgacNcs;
    }

    protected Raexport(byte[] bytes, int offset) {
        setBytes(bytes, offset);
    }

    protected Raexport(byte[] bytes) {
        this(bytes, 0);
    }

    public static Raexport fromBytes(byte[] bytes, int offset) {
        return new Raexport(bytes, offset);
    }

    public static Raexport fromBytes(byte[] bytes) {
        return fromBytes(bytes, 0);
    }

    public static Raexport fromBytes(String bytes) {
        try {
            return fromBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public long getDb2CustomernumInt() {
        return this.db2CustomernumInt;
    }

    public void setDb2CustomernumInt(long db2CustomernumInt) {
        this.db2CustomernumInt = db2CustomernumInt;
    }

    public String getLgacNcs() {
        return this.lgacNcs;
    }

    public void setLgacNcs(String lgacNcs) {
        this.lgacNcs = lgacNcs;
    }

    public void reset() {
        db2CustomernumInt = 0;
        lgacNcs = "";
    }

    public static void mainlineFirstSentences(Object pfmZ7vxO9A1585, LocalDateTime qlbgoMts2zzprA1585, LocalDateTime uaugpwvqUwtA1585, int gkxvdfbuTarb40Cmi65b5A1585, Object smymE1uA1585) {
    Cdb2area cdb2area = new Cdb2area();
    Dfhcommarea1 dfhcommarea1 = new Dfhcommarea1();
    ErrorMsg errorMsg = new ErrorMsg();
    String lgacdb02 = "LGACDB02";
    String lgacvs01 = "LGACVS01";
    WsCommareaLengths wsCommareaLengths = new WsCommareaLengths();
    int wsCustomerLen = 72;
    WsHeader wsHeader = new WsHeader();
    /* Unable to translate CICS API due to use of an unsupported option */
    wsHeader.setWsTransid(Task.getTask().getTransactionName());
    wsHeader.setWsTermid(Task.getTask().getInvokingProgramName());
    wsHeader.setWsTasknum((int) Task.getTask().getTaskNumber());
    wsCommareaLengths.setWsCaHeaderLen(225);
    wsCommareaLengths.setWsRequiredCaLen(wsCommareaLengths.getWsCaHeaderLen() + wsCustomerLen);
    if (dfhcommarea1.getEibcalen() < wsCommareaLengths.getWsRequiredCaLen()) {
        errorMsg.setEmVariable(" NO COMMAREA RECEIVED");
        errorMsg.mainlineWriteErrorMessage(dfhcommarea1);
        Task.getTask().abend("LGCA", false);
    }
    dfhcommarea1.setCaReturnCode(Integer.parseInt("00"));
    wsHeader.setWsCalen(dfhcommarea1.getEibcalen());
    wsHeader.setWsAddrDfhcommarea(dfhcommarea1.getEibcalen());
    // TODO: 'db2CustomernumInt' not declared locally. Please verify it exists.
    cdb2area.setD2CustomerNum(db2CustomernumInt);
    cdb2area.setD2RequestId("02ACUS");
    cdb2area.setD2CustsecrPass("<PASSWORD>");
    cdb2area.setD2CustsecrCount("0000");
    cdb2area.setD2CustsecrState('N');
    try {
        java.time.format.DateTimeFormatter jdeclDateFormat = java.time.format.DateTimeFormatter
                .ofPattern("yyyyMMddHHmmssSSZ");
        java.time.ZonedDateTime jdeclDateTime = java.time.ZonedDateTime.of(1582, 10, 14, 0, 0, 0, 0,
                java.time.ZoneOffset.UTC);
        jdeclDateTime = jdeclDateTime.plusDays(143951);
        jdeclDateTime = jdeclDateTime.plusSeconds(18867);
        jdeclDateTime = jdeclDateTime.plusNanos(812479168);

        cdb2area.setD2CustsecrDate(jdeclDateFormat.format(jdeclDateTime));

    } catch (Exception e) {
        System.out.println("FOUND EXCEPTION");
    }
    try {
        // TODO: 'pfmZ7vxO9A1585' not declared locally. Please verify it exists.
        // TODO: 'rhshcxuCA1585' not declared locally. Please verify it exists.
        // inlined call to CEELOCT
        pfmZ7vxO9A1585 = java.time.format.DateTimeFormatter.ofPattern(lgacdb02, java.util.Locale.ENGLISH);
        // TODO: 'qlbgoMts2zzprA1585' not declared locally. Please verify it exists.
        qlbgoMts2zzprA1585 = java.time.LocalDateTime.of(1582, 10, 14, 0, 0, 0, 0);
        // TODO: 'uaugpwvqUwtA1585' not declared locally. Please verify it exists.
        uaugpwvqUwtA1585 = java.time.LocalDateTime.now();

        // TODO: 'gkxvdfbuTarb40Cmi65b5A1585' not declared locally. Please verify it exists.
        // TODO: 'uaugpwvqUwtA1585' not declared locally. Please verify it exists.
        gkxvdfbuTarb40Cmi65b5A1585 = (int) qlbgoMts2zzprA1585.until(uaugpwvqUwtA1585,
                java.time.temporal.ChronoUnit.DAYS);
        // TODO: 'ypmteGLhfoA1585' not declared locally. Please verify it exists.
        // TODO: 'uaugpwvqUwtA1585' not declared locally. Please verify it exists.
        double ypmteGLhfoA1585 = ((double) qlbgoMts2zzprA1585.until(uaugpwvqUwtA1585, java.time.temporal.ChronoUnit.MILLIS)
                / 1000d);
        // TODO: 'smymE1uA1585' not declared locally. Please verify it exists.
        // TODO: 'uaugpwvqUwtA1585' not declared locally. Please verify it exists.
        smymE1uA1585 = ((LocalDateTime) pfmZ7vxO9A1585).format(uaugpwvqUwtA1585);
        // end of inlined call to CEELOCT

        System.out.println("Testcase1 Successfull");
        // TODO: 'smymE1uA1585' not declared locally. Please verify it exists.
        System.out.println("Local Time is " + smymE1uA1585);
        // Example result: Local Time is 20231122143736007
        // Ran on 11/22/2023 at 14:37:36.007
    } catch (Exception e) {
        System.out.println("Testcase1 Failed");
        System.out.println("CEELOCT failed with msg " + e.getMessage());
    }
    try {
        // sql insert with default
        String sql0 = "insert into Customer(FIRSTNAME, LASTNAME, DATEOFBIRTH, HOUSENAME, HOUSENUMBER, POSTCODE, PHONEMOBILE, PHONEHOME, EMAILADDRESS) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps0 = DriverManager.getConnection("endpoint_url").prepareStatement(sql0,
                Statement.RETURN_GENERATED_KEYS);
        ps0.setString(1, cdb2area.getD2CustsecrState());
        ps0.setString(2, cdb2area.getD2CustsecrCount());
        ps0.setString(3, cdb2area.getD2CustsecrDate());
        ps0.setString(4, cdb2area.getD2RequestId());
        ps0.setDouble(5, wsCommareaLengths.getWsCaHeaderLen());
        ps0.setString(6, wsHeader.getWsTransid());
        ps0.setInt(7, wsCommareaLengths.getWsCaHeaderLen());
        ps0.setString(8, wsHeader.getWsTermid());
        ps0.setInt(9,wsCommareaLengths.getWsRequiredCaLen());

        ps0.executeUpdate();
        ResultSet rs = ps0.getGeneratedKeys();
        rs.next();
        // TODO: 'db2CustomernumInt' not declared locally. Please verify it exists.
        db2CustomernumInt = rs.getInt(1);
        ps0.close();
    } catch (Exception exception) {
        // caReturnCode = 90;
        // errorMsg.writeErrorMessage();
        System.out.println(exception);
    }
    Task jdeclTask = Task.getTask();
    Channel jdeclChannelObj = (Channel) jdeclTask.getCurrentChannel();
    Container jdeclContainerObj = ((com.ibm.cics.server.Channel) jdeclChannelObj).getContainer(wsHeader.getWsAddrDfhcommarea());
    byte[] jdeclTempOutputData = jdeclContainerObj.get();
    Charset jdeclCharset = Charset.forName(System.getProperty("com.ibm.cics.jvmserver.local.ccsid"));
    dfhcommarea1.setBytes(new String(jdeclTempOutputData, jdeclCharset));
    if (dfhcommarea1.getCaReturnCode() != 0) {
        Task.getTask().abend();
    }
}


    public static void mainlineObtainCustomerNumber() {
        String genacount = "GENACUSTNUM";
        String genapool = "GENA";
        String lastCustNum;
        String wsResp;
        wsResp = Task.getTask().getUSERID();
        if (wsResp == 0) {
            lastCustNum = Task.getTask().getUSERID();
        } else {
            // TODO: 'lgacNcs' not declared locally. Please verify it exists.
            lgacNcs = "NO";
            // TODO: 'db2CustomernumInt' not declared locally. Please verify it exists.
            db2CustomernumInt = 0;
        }
    }

    public static void mainlineInsertCustomer(CaCustomerRequest caCustomerRequest) {
    ErrorMsg errorMsg = new ErrorMsg();
    try {
        String sql = "INSERT INTO CUSTOMER(CUSTOMERNUMBER, FIRSTNAME, LASTNAME, DATEOFBIRTH, HOUSENAME, HOUSENUMBER, POSTCODE, PHONEMOBILE, PHONEHOME, EMAILADDRESS)values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = ((Connection) JdbcConnection.connection).prepareStatement(sql);
        ps.setLong(1, caCustomerRequest.getCaCustomerNum());
        ps.setString(2, caCustomerRequest.getCaFirstName());
        ps.setString(3, caCustomerRequest.getCaLastName());
        ps.setString(4, caCustomerRequest.getCaDob());
        ps.setString(5, caCustomerRequest.getCaHouseName());
        ps.setString(6, caCustomerRequest.getCaHouseNum());
        ps.setString(7, caCustomerRequest.getCaPostcode());
        ps.setString(8, caCustomerRequest.getCaPhoneMobile());
        ps.setString(9, caCustomerRequest.getCaPhoneHome());
        ps.setString(10, caCustomerRequest.getCaEmailAddress());
        ps.executeUpdate();
        ps.close();
    } catch (SQLException exception) {
        errorMsg.getEmVariable().setEmSqlreq(exception.getErrorCode() + "");
        Dfhcommarea1 dfhcommarea1;
        // TODO: 'dfhcommarea1' not declared locally. Please verify it exists.
        errorMsg.mainlineWriteErrorMessage(dfhcommarea1);
        return;
    }
}


    public static void main(String[] args) {
        mainlineFirstSentences(args, null, null, 0, args);
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("{ db2CustomernumInt=\"");
        s.append(getDb2CustomernumInt());
        s.append("\"");
        s.append(", lgacNcs=\"");
        s.append(getLgacNcs());
        s.append("\"");
        s.append("}");
        return s.toString();
    }

    public boolean equals(Raexport that) {
        return this.db2CustomernumInt == that.db2CustomernumInt &&
                this.lgacNcs.equals(that.lgacNcs);
    }

    @Override
    public boolean equals(Object that) {
        return (that instanceof Raexport) && this.equals((Raexport) that);
    }

    @Override
    public int hashCode() {
        return Long.hashCode(db2CustomernumInt) ^
                Integer.rotateLeft(lgacNcs.hashCode(), 1);
    }

    @Override
    public int compareTo(Raexport that) {
        int c = 0;
        c = Long.compare(this.db2CustomernumInt, that.db2CustomernumInt);
        if (c != 0)
            return c;
        c = this.lgacNcs.compareTo(that.lgacNcs);
        return c;
    }

    // Start of COBOL-compatible binary serialization metadata
    private static CobolDatatypeFactory factory = new CobolDatatypeFactory();
    static {
        factory.setStringTrimDefault(true);
        factory.setStringEncoding("IBM-1047");
    }

    private static final BinaryAsLongField DB_2_CUSTOMERNUM_INT = factory.getBinaryAsLongField(10, true);
    private static final StringField LGAC_NCS = factory.getStringField(2);
    public static final int SIZE = factory.getOffset();
    // End of COBOL-compatible binary serialization metadata

    public byte[] getBytes(byte[] bytes, int offset) {
        DB_2_CUSTOMERNUM_INT.putLong(db2CustomernumInt, bytes, offset);
        LGAC_NCS.putString(lgacNcs, bytes, offset);
        return bytes;
    }

    public final byte[] getBytes(byte[] bytes) {
        return getBytes(bytes, 0);
    }

    public final byte[] getBytes() {
        return getBytes(new byte[numBytes()]);
    }

    public final String toByteString() {
        try {
            return new String(getBytes(), factory.getStringEncoding());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void setBytes(byte[] bytes, int offset) {
        if (bytes.length < SIZE + offset) {
            byte[] newBytes = Arrays.copyOf(bytes, SIZE + offset);
            Arrays.fill(newBytes, bytes.length, SIZE + offset, (byte) 0x40 /* default EBCDIC space character */);
            bytes = newBytes;
        }
        db2CustomernumInt = DB_2_CUSTOMERNUM_INT.getLong(bytes, offset);
        lgacNcs = LGAC_NCS.getString(bytes, offset);
    }

    public final void setBytes(byte[] bytes) {
        setBytes(bytes, 0);
    }

    public final void setBytes(String bytes) {
        try {
            setBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public int numBytes() {
        return SIZE;
    }

}
