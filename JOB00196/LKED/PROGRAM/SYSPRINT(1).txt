1z/OS V2 R5 BINDER     14:49:10 WEDNESDAY MAY 22, 2024
 BATCH EMULATOR  JOB(COMP    ) STEP(PROGRAM ) PGM= HEWL      PROCEDURE(LKED    )
 IEW2278I B352 INVOCATION PARAMETERS - LIST,XREF,RENT,NAME=RALGACUS
 IEW2322I 1220  1     INCLUDE SYSLIB(DFHECI)
 IEW2322I 1220  2     INCLUDE SYSLIB(DSNCLI)


1                                       C R O S S - R E F E R E N C E  T A B L E
                                        _________________________________________

 TEXT CLASS = C_CODE

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
       24 RALGACUS                       24 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       2C RALGACUS                       2C A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       30 RALGACUS                       30 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       34 RALGACUS                       34 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       38 RALGACUS                       38 V-CON | IGZXBST4         IGZXBST4                        0 B_TEXT           |
       3C RALGACUS                       3C A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       44 RALGACUS                       44 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
     1094 RALGACUS                     1094 Q-CON | RALGACUS#S       $NON-RELOCATABLE               10 C_WSA            |
     1098 RALGACUS                     1098 V-CON | DFHEI1           DFHECI                          8 B_TEXT           |
     109C RALGACUS                     109C V-CON | IGZXFCAL         IGZXFCAL                        0 B_TEXT           |
     10A0 RALGACUS                     10A0 V-CON | IGZXPRS          IGZXPRS                         0 B_TEXT           |
     10A4 RALGACUS                     10A4 V-CON | IGZXCMSG         IGZXCMSG                        0 B_TEXT           |
     10A8 RALGACUS                     10A8 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
     10AC RALGACUS                     10AC A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
     10B0 RALGACUS                     10B0 V-CON | IGZXVMO          IGZXVMO                         0 B_TEXT           |
     10B4 RALGACUS                     10B4 V-CON | IGZXDSP          IGZXDSP                         0 B_TEXT           |
     10B8 RALGACUS                     10B8 V-CON | IGZXRT1          IGZXRT1                         0 B_TEXT           |
     10DC RALGACUS                     10DC A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
     10E8 RALGACUS                     10E8 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
     1190 RALGACUS                     1190 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
     14AC RALGACUS                     14AC A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
     178C RALGACUS                     178C A-CON | CEESTART         CEESTART                        0 B_TEXT           |
     178C RALGACUS                     178C A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
     17A4 RALGACUS                     17A4 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
     17A8 RALGACUS                     17A8 V-CON | CEESTART         CEESTART                        0 B_TEXT           |
     17AC RALGACUS                     17AC V-CON | CEEBETBL         CEEBETBL                        0 B_TEXT           |
     17FC RALGACUS                     17FC Q-CON | RALGACUS#S       $NON-RELOCATABLE               10 C_WSA            |


 TEXT CLASS = B_TEXT

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
       B4 CEESG004                       14 A-CON | IGZEOPT          $UNRESOLVED(W)                                     |
       B8 CEESG004                       18 A-CON | IGZETUN          $UNRESOLVED(W)                                     |
       BC CEESG004                       1C A-CON | IGZXRES1         $UNRESOLVED(W)                                     |
       C0 CEESG004                       20 A-CON | IEWBLIT          IEWBLIT                         0 B_LIT            |
      5B8 IGZXBST4                      450 V-CON | CEEINT           CEEBPIRA                        0 B_TEXT           |
      5BC IGZXBST4                      454 V-CON | CEEARLU          CEEARLU                         0 B_TEXT           |
      5C0 IGZXBST4                      458 A-CON | CEESG004         CEESG004                        0 B_TEXT           |
      64C CEEBETBL                        4 V-CON | CEEBXITA         $UNRESOLVED(W)                                     |
      650 CEEBETBL                        8 V-CON | CEEBINT          CEEBINT                         0 B_TEXT           |
      654 CEEBETBL                        C V-CON | CEEBLLST         CEEBLLST                        0 B_TEXT           |
      658 CEEBETBL                       10 V-CON | CEEUOPT          $UNRESOLVED(W)                                     |
      65C CEEBETBL                       14 V-CON | CEEBTRM          CEEBTRM                         0 B_TEXT           |


1                                       C R O S S - R E F E R E N C E  T A B L E
                                        _________________________________________

 TEXT CLASS = B_TEXT

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
      664 CEEBETBL                       1C V-CON | CEEBPUBT         CEEBPUBT                        0 B_TEXT           |
      668 CEEBETBL                       20 V-CON | IEWBLIT          IEWBLIT                         0 B_LIT            |
      6CC CEESTART                       5C A-CON | CEEMAIN          $UNRESOLVED(W)                                     |
      6DC CEESTART                       6C C-LEN |                  $NON-RELOCATABLE                  B_PRV            |
      708 CEESTART                       98 A-CON | CEEFMAIN         $UNRESOLVED(W)                                     |
      714 CEESTART                       A4 A-CON | CEEBETBL         CEEBETBL                        0 B_TEXT           |
      718 CEESTART                       A8 A-CON | CEEROOTA         $UNRESOLVED(W)                                     |
      71C CEESTART                       AC A-CON | CEEROOTD         $UNRESOLVED(W)                                     |
      7FC CEEBTRM                        6C A-CON | CEEBPUBT         CEEBPUBT                        0 B_TEXT           |
      8E8 CEEBLLST                       10 A-CON | CEESG000         $UNRESOLVED(W)                                     |
      8EC CEEBLLST                       14 A-CON | CEESG001         $UNRESOLVED(W)                                     |
      8F0 CEEBLLST                       18 A-CON | CEESG002         $UNRESOLVED(W)                                     |
      8F4 CEEBLLST                       1C A-CON | CEESG003         $UNRESOLVED(W)                                     |
      8F8 CEEBLLST                       20 A-CON | CEESG004         CEESG004                        0 B_TEXT           |
      8FC CEEBLLST                       24 A-CON | CEESG005         $UNRESOLVED(W)                                     |
      900 CEEBLLST                       28 A-CON | CEESG006         $UNRESOLVED(W)                                     |
      904 CEEBLLST                       2C A-CON | CEESG007         $UNRESOLVED(W)                                     |
      908 CEEBLLST                       30 A-CON | CEESG008         $UNRESOLVED(W)                                     |
      90C CEEBLLST                       34 A-CON | CEESG009         $UNRESOLVED(W)                                     |
      910 CEEBLLST                       38 A-CON | CEESG010         $UNRESOLVED(W)                                     |
      914 CEEBLLST                       3C A-CON | CEESG011         $UNRESOLVED(W)                                     |
      918 CEEBLLST                       40 A-CON | CEESG012         $UNRESOLVED(W)                                     |
      91C CEEBLLST                       44 A-CON | CEESG013         $UNRESOLVED(W)                                     |
      920 CEEBLLST                       48 A-CON | CEESG014         $UNRESOLVED(W)                                     |
      924 CEEBLLST                       4C A-CON | CEESG015         $UNRESOLVED(W)                                     |
      928 CEEBLLST                       50 A-CON | CEESG016         $UNRESOLVED(W)                                     |
      C28 CEEBPIRA                      230 A-CON | CEEBPUBT         CEEBPUBT                        0 B_TEXT           |


 TEXT CLASS = C_@@PPA2

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        4 $PRIV00001A                     4 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |


 TEXT CLASS = C_@@CSINIT

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        0 $PRIV00001B                     0 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |


 TEXT CLASS = B_LIT

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
1  OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
       28 IEWBLIT                        28 L TOKE|                                                                     |


1                                       C R O S S - R E F E R E N C E  T A B L E
                                        _________________________________________

 TEXT CLASS = B_LIT

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
       50 IEWBLIT                        50 C-LEN |                  $NON-RELOCATABLE                  B_TEXT           |
       54 IEWBLIT                        54 A-CON |                                                    B_TEXT           |
       70 IEWBLIT                        70 C-LEN |                  $NON-RELOCATABLE                  C_CODE           |
       74 IEWBLIT                        74 A-CON |                                                    C_CODE           |
       90 IEWBLIT                        90 C-LEN |                  $NON-RELOCATABLE                  C_WSA            |
       B0 IEWBLIT                        B0 C-LEN |                  $NON-RELOCATABLE                  C_@@PPA2         |
       B4 IEWBLIT                        B4 A-CON |                                                    C_@@PPA2         |
       D0 IEWBLIT                        D0 C-LEN |                  $NON-RELOCATABLE                  C_@@CSINIT       |
       D4 IEWBLIT                        D4 A-CON |                                                    C_@@CSINIT       |
       F0 IEWBLIT                        F0 C-LEN |                  $NON-RELOCATABLE                  B_LIT            |
       F4 IEWBLIT                        F4 A-CON |                                                    B_LIT            |


 TEXT CLASS = C_WSA

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
       24 RALGACUS#S                     24 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       28 RALGACUS#S                     28 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       2C RALGACUS#S                     2C A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       30 RALGACUS#S                     30 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       34 RALGACUS#S                     34 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       40 RALGACUS#S                     40 A-CON | RALGACUS#S       $PRIV000003                    10 C_WSA            |
       44 RALGACUS#S                     44 A-CON | RALGACUS#C       RALGACUS                        0 C_CODE           |
       8C RALGACUS#S                     8C V-CON | RALGACUS         RALGACUS                        0 C_CODE           |
       A0 RALGACUS#S                     A0 A-CON | RALGACUS#S       $PRIV000003                    10 C_WSA            |


 TEXT CLASS = D_INFO

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        6 RALGACUS                        6 Q-CON | $PRIV000012      $NON-RELOCATABLE                0 D_ABREV          |
       51 RALGACUS                       51 Q-CON | $PRIV000016      $NON-RELOCATABLE                B D_SRCF           |
       55 RALGACUS                       55 Q-CON | RALGACUS#C       $NON-RELOCATABLE                0 C_CODE           |
       59 RALGACUS                       59 Q-CON | RALGACUS#C       $NON-RELOCATABLE             1840 C_CODE           |
       61 RALGACUS                       61 Q-CON | $PRIV000011      $NON-RELOCATABLE                0 D_LINE           |
      3B2 RALGACUS                      3B2 Q-CON | RALGACUS#C       $NON-RELOCATABLE                0 C_CODE           |
      3B6 RALGACUS                      3B6 Q-CON | RALGACUS#C       $NON-RELOCATABLE              A80 C_CODE           |
     15A0 RALGACUS                     15A0 Q-CON | RALGACUS#C       $NON-RELOCATABLE              194 C_CODE           |
     15B9 RALGACUS                     15B9 Q-CON | RALGACUS#C       $NON-RELOCATABLE              4F4 C_CODE           |
     15D2 RALGACUS                     15D2 Q-CON | RALGACUS#C       $NON-RELOCATABLE              574 C_CODE           |
     15F1 RALGACUS                     15F1 Q-CON | RALGACUS#C       $NON-RELOCATABLE              598 C_CODE           |


1                                       C R O S S - R E F E R E N C E  T A B L E
                                        _________________________________________

 TEXT CLASS = D_INFO

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
     160E RALGACUS                     160E Q-CON | RALGACUS#C       $NON-RELOCATABLE              668 C_CODE           |
     162F RALGACUS                     162F Q-CON | RALGACUS#C       $NON-RELOCATABLE              718 C_CODE           |
     33E3 RALGACUS                     33E3 Q-CON | RALGACUS#C       $NON-RELOCATABLE             17FC C_CODE           |
     33EE RALGACUS                     33EE Q-CON | RALGACUS#C       $NON-RELOCATABLE             17FC C_CODE           |
     340A RALGACUS                     340A Q-CON | $PRIV000016      $NON-RELOCATABLE               83 D_SRCF           |
     340E RALGACUS                     340E Q-CON | $PRIV000011      $NON-RELOCATABLE              420 D_LINE           |


 TEXT CLASS = D_LINE

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
       20 RALGACUS                       20 Q-CON | $PRIV000016      $NON-RELOCATABLE                B D_SRCF           |
       27 RALGACUS                       27 Q-CON | RALGACUS#C       $NON-RELOCATABLE                0 C_CODE           |
      440 RALGACUS                      440 Q-CON | $PRIV000016      $NON-RELOCATABLE               83 D_SRCF           |
      447 RALGACUS                      447 Q-CON | RALGACUS#C       $NON-RELOCATABLE                0 C_CODE           |


 TEXT CLASS = D_ABREV

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |


 TEXT CLASS = D_ARNGE

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        6 RALGACUS                        6 Q-CON | $PRIV000010      $NON-RELOCATABLE                0 D_INFO           |
       10 RALGACUS                       10 Q-CON | RALGACUS#C       $NON-RELOCATABLE                0 C_CODE           |


 TEXT CLASS = D_PBNMS

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        6 RALGACUS                        6 Q-CON | $PRIV000010      $NON-RELOCATABLE                0 D_INFO           |


 TEXT CLASS = D_PPA

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
1  OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        6 RALGACUS                        6 Q-CON | $PRIV000012      $NON-RELOCATABLE              26E D_ABREV          |


1                                       C R O S S - R E F E R E N C E  T A B L E
                                        _________________________________________

 TEXT CLASS = D_PPA

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        C RALGACUS                        C Q-CON | RALGACUS#C       $NON-RELOCATABLE             1788 C_CODE           |
       10 RALGACUS                       10 Q-CON | $PRIV000010      $NON-RELOCATABLE                B D_INFO           |
       3E RALGACUS                       3E Q-CON | RALGACUS#C       $NON-RELOCATABLE             14D0 C_CODE           |
       42 RALGACUS                       42 Q-CON | $PRIV000010      $NON-RELOCATABLE              3A4 D_INFO           |


 TEXT CLASS = D_SRCF

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        6 RALGACUS                        6 Q-CON | $PRIV000012      $NON-RELOCATABLE              214 D_ABREV          |
       2B RALGACUS                       2B Q-CON | $PRIV000017      $NON-RELOCATABLE                0 D_SRCTXT         |
       40 RALGACUS                       40 Q-CON | $PRIV000018      $NON-RELOCATABLE                0 D_SRCATR         |
       4E RALGACUS                       4E Q-CON | $PRIV000012      $NON-RELOCATABLE              22D D_ABREV          |
       7E RALGACUS                       7E Q-CON | $PRIV000012      $NON-RELOCATABLE              237 D_ABREV          |
       84 RALGACUS                       84 Q-CON | $PRIV000016      $NON-RELOCATABLE               53 D_SRCF           |
       9E RALGACUS                       9E Q-CON | $PRIV000012      $NON-RELOCATABLE              24A D_ABREV          |
       A4 RALGACUS                       A4 Q-CON | $PRIV000016      $NON-RELOCATABLE               53 D_SRCF           |
       BE RALGACUS                       BE Q-CON | $PRIV000012      $NON-RELOCATABLE              25C D_ABREV          |
       C4 RALGACUS                       C4 Q-CON | $PRIV000016      $NON-RELOCATABLE               53 D_SRCF           |


 TEXT CLASS = D_SRCTXT

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |


 TEXT CLASS = D_SRCATR

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        C RALGACUS                        C Q-CON | $PRIV000019      $NON-RELOCATABLE                0 D_XREF           |


 TEXT CLASS = D_XREF

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        6 RALGACUS                        6 Q-CON | $PRIV000012      $NON-RELOCATABLE              289 D_ABREV          |


1                                       C R O S S - R E F E R E N C E  T A B L E
                                        _________________________________________

 TEXT CLASS = D_XREF

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
        C RALGACUS                        C Q-CON | $PRIV000016      $NON-RELOCATABLE                B D_SRCF           |
       10 RALGACUS                       10 Q-CON | $PRIV000010      $NON-RELOCATABLE                B D_INFO           |


 TEXT CLASS = D_MCODE

 ---------------  R E F E R E N C E  --------------------------  T A R G E T  -------------------------------------------
   CLASS                            ELEMENT       |                                            ELEMENT                  |
   OFFSET SECT/PART(ABBREV)          OFFSET  TYPE | SYMBOL(ABBREV)   SECTION (ABBREV)           OFFSET CLASS NAME       |
                                                  |                                                                     |
                                        *** E N D  O F  C R O S S  R E F E R E N C E ***



                                 *** O P E R A T I O N   S U M M A R Y   R E P O R T ***

1PROCESSING OPTIONS:

    ALIASES             NO
    ALIGN2              NO
    AMODE               UNSPECIFIED
    CALL                YES
    CASE                UPPER
    COMPAT              UNSPECIFIED
    COMPRESS            AUTO
    DCBS                NO
    DYNAM               NO
    EXTATTR             UNSPECIFIED
    EXITS:              NONE
    FILL                NONE
    GID                 UNSPECIFIED
    HOBSET              NO
    INFO                NO
    LET                 04
    LINECT              060
    LIST                SUMMARY
    LISTPRIV            NO
    LONGPARM            NO
    MAP                 NO
    MAXBLK              032760
    MODMAP              NO
    MSGLEVEL            00
    OVLY                NO
    PRINT               YES
    RES                 NO
    REUSABILITY         REENTRANT
    RMODE               UNSPECIFIED
    RMODEX              NO
    SIGN                NO
    STORENX             NOREPLACE
    STRIPCL             NO
    STRIPSEC            NO
    SYMTRACE
    TERM                NO
    TRAP                ON
    UID                 UNSPECIFIED
    UPCASE              NO
    WKSPACE             000000K,000000K
    XCAL                NO
    XREF                YES
    ***END OF OPTIONS***




1SAVE OPERATION SUMMARY:

    MEMBER NAME         RALGACUS
    LOAD LIBRARY        IBMUSER.CB12V51.LOAD
    PROGRAM TYPE        PROGRAM OBJECT(FORMAT 3)
    VOLUME SERIAL       USRVS1
    DISPOSITION         ADDED NEW
    TIME OF SAVE        14.49.10  MAY 22, 2024


1SAVE MODULE ATTRIBUTES:

    AC                  000
    AMODE                31
    COMPRESSION         NONE
    DC                  NO
    EDITABLE            YES
    EXCEEDS 16MB        NO
    EXECUTABLE          YES
    LONGPARM            NO
    MIGRATABLE          NO
    OL                  NO
    OVLY                NO
    PACK,PRIME          NO,NO
    PAGE ALIGN          NO
    REFR                NO
    RENT                YES
    REUS                YES
    RMODE               ANY
    SCTR                NO
    SIGN                NO
    SSI
    SYM GENERATED       NO
    TEST                NO
    XPLINK              NO
    MODULE SIZE (HEX)   000026A8
    DASD SIZE (HEX)     00017000


1 ENTRY POINT AND ALIAS SUMMARY:

  NAME:            ENTRY TYPE AMODE C_OFFSET CLASS NAME        STATUS

  RALGACUS          MAIN_EP      31 00000000 C_CODE

                          *** E N D   O F   O P E R A T I O N   S U M M A R Y   R E P O R T ***




1z/OS V2 R5 BINDER     14:49:10 WEDNESDAY MAY 22, 2024
 BATCH EMULATOR  JOB(COMP    ) STEP(PROGRAM ) PGM= HEWL      PROCEDURE(LKED    )
 IEW2008I 0F03 PROCESSING COMPLETED.  RETURN CODE =  0.



1----------------------
 MESSAGE SUMMARY REPORT
 ----------------------
  TERMINAL MESSAGES      (SEVERITY = 16)
  NONE

  SEVERE MESSAGES        (SEVERITY = 12)
  NONE

  ERROR MESSAGES         (SEVERITY = 08)
  NONE

  WARNING MESSAGES       (SEVERITY = 04)
  NONE

  INFORMATIONAL MESSAGES (SEVERITY = 00)
  2008  2278  2322


  **** END OF MESSAGE SUMMARY REPORT ****

