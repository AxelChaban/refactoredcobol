//ZDERUN  JOB ,NOTIFY=&SYSUID,
// MSGCLASS=H,MSGLEVEL=(1,1),REGION=144M
//*****************************************************************
//*
//* LICENSED MATERIALS - PROPERTY OF IBM
//* "RESTRICTED MATERIALS OF IBM"
//* (C) COPYRIGHT IBM CORPORATION 2023, 2024. ALL RIGHTS RESERVED
//*
//*  US GOVERNMENT USERS RESTRICTED RIGHTS - USE, DUPLICATION,
//*  OR DISCLOSURE RESTRICTED BY GSA ADP SCHEDULE
//*  CONTRACT WITH IBM CORPORATION
//*
//* DONOT RUN THIS JCL WITHOUT MODIFICATION
//* 1) Uncomment and add IBM Debug SEQAMOD library and the LE
//*    libraries if not part of the Linklist in the SET statement
//*    and STEPLIB
//* 2) Modify the joblib
//*
//*****************************************************************
//* VARIABLE DEFINITIONS
//*****************************************************************
//*
//    SET PGMNAME='RALGACUS'             *PROGRAM NAME
//    SET APPLIB='IBMUSER.CB12V51.LOAD'
//*   SET EQALIB='EQAW.SEQAMOD'               *DEBUG LIBRARY
//*   SET LE1='CEE.SCEERUN'                   *LE libraries
//*   SET LE2='CEE.SCEERUN2'                  *LE libraries
//*
//*   SET TESTOPT='/TEST(, WVCJIN,,)'
//*
//*************************
//* CLEAN UP
//*************************
//DELETE   EXEC PGM=IEFBR14
//SYSPRINT DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
/*
//*************************
//* RUN PROGRAM
//*************************
//WVCJLCY EXEC PGM=&PGMNAME
//STEPLIB DD DSN=&APPLIB,DISP=SHR
//*       DD   DISP=SHR,DSN=&LE1
//*       DD   DISP=SHR,DSN=&LE2
//*       DD   DISP=SHR,DSN=&EQALIB
//* Add any other application libraries here
//CEEOPTS DD *
 TEST(, WVCJIN,,)
/*
//WVCJIN  DD *
       DESCRIBE CUS *;
       LIST %LOAD;
       LIST %CU;
       LIST %BLOCK;
       LIST LINE NUMBERS;
       AT EXIT * BEGIN;
       LIST 'END TEST_ID 3 FAILED: UNEXPECTED EXIT';
       LIST TITLED WSS;
       QUIT (1);
       END;
       01 WCAZVA-COMMAREA PIC X(32768);
       AT 301 BEGIN;
       LIST 'PATH LINE 301';
       LIST 'BEGIN SETUP COMMAREA, IGNORE ERRORS TO END OF SETUP';
       SET ADDRESS OF DFHCOMMAREA TO ADDRESS OF WCAZVA-COMMAREA;
       MOVE 32768 TO EIBCALEN OF DFHEIBLK;
       LIST 'END SETUP COMMAREA';
       LIST 'SET PROGRAM INPUTS FOR 301';
       MOVE X'A7A7A7A7' TO EIBTRNID OF DFHEIBLK;
       GO;
       END;
       AT 302 BEGIN;
       LIST 'PATH LINE 302';
       LIST 'SET PROGRAM INPUTS FOR 302';
       MOVE X'A7A7A7A7' TO EIBTRMID OF DFHEIBLK;
       GO;
       END;
       AT 303 BEGIN;
       LIST 'PATH LINE 303';
       LIST 'SET PROGRAM INPUTS FOR 303';
       MOVE 0 TO EIBTASKN OF DFHEIBLK;
       GO;
       END;
       AT 310 BEGIN;
       LIST 'PATH LINE 310';
       LIST 'SET PROGRAM INPUTS FOR 310';
       MOVE 800 TO EIBCALEN OF DFHEIBLK;
       GO;
       END;
       AT 323 BEGIN;
       LIST 'PATH LINE 323';
       LIST 'SET PROGRAM INPUTS FOR 323';
       MOVE 0 TO WS-REQUIRED-CA-LEN OF WS-COMMAREA-LENGTHS;
       MOVE 0 TO WS-CA-HEADER-LEN OF WS-COMMAREA-LENGTHS;
       GO;
       END;
       AT 324 BEGIN;
       LIST 'PATH LINE 324';
       LIST 'SET PROGRAM INPUTS FOR 324';
       MOVE 0 TO WS-CUSTOMER-LEN OF WS-POLICY-LENGTHS;
       GO;
       END;
       AT 336 BEGIN;
       LIST 'PATH LINE 336';
       GOTO 337;
       END;
       AT 337 BEGIN;
       LIST 'PATH LINE 337';
       LIST 'SET PROGRAM INPUTS FOR 337';
       MOVE 0 TO WS-RESPONSE-CODE OF WS-RESPONSE;
       GO;
       END;
       AT 343 BEGIN;
       LIST 'PATH LINE 343';
       GOTO 344;
       END;
       AT 344 BEGIN;
       LIST 'PATH LINE 344';
       LIST 'SET PROGRAM INPUTS FOR 344';
       MOVE X'E5C1D3C9C4' TO WS-STATUS OF WS-JAVA-VARIABLES;
       GO;
       END;
       AT 353 BEGIN;
       LIST 'PATH LINE 353';
       GOTO 354;
       END;
       AT 360 BEGIN;
       LIST 'PATH LINE 360';
       LIST 'SKIPPING EXECUTION OF 360';
       LIST 'BEGIN PROGRAM OUTPUTS FOR 360';
       LIST 'BEGIN VAR';
       LIST TITLED %HEX(WS-EYECATCHER OF WS-HEADER);
       LIST 'END VAR';
       LIST 'BEGIN VAR';
       LIST TITLED %HEX(WS-FILLER OF WS-HEADER);
       LIST 'END VAR';
       LIST 'BEGIN VAR';
       LIST TITLED %HEX(WS-TRANSID OF WS-HEADER);
       LIST 'END VAR';
       LIST 'BEGIN VAR';
       LIST TITLED %HEX(WS-TERMID OF WS-HEADER);
       LIST 'END VAR';
       LIST 'BEGIN VAR';
       LIST TITLED WS-TASKNUM OF WS-HEADER;
       LIST 'END VAR';
       LIST 'BEGIN VAR';
       LIST TITLED CA-RETURN-CODE OF DFHCOMMAREA-1;
       LIST 'END VAR';
       LIST 'BEGIN VAR';
       LIST TITLED CA-NUM-POLICIES OF CA-CUSTOMER-REQUEST OF DFHCOMMAREA
      --1;
       LIST 'END VAR';
       LIST 'BEGIN VAR';
       LIST TITLED WS-CALEN OF WS-HEADER;
       LIST 'END VAR';
       LIST 'BEGIN VAR';
       LIST TITLED WS-REQUIRED-CA-LEN OF WS-COMMAREA-LENGTHS;
       LIST 'END VAR';
       LIST 'END PROGRAM OUTPUTS FOR 360';
       LIST 'END TEST_ID 3 AT 360';
       QUIT (0);
       END;
       LIST 'BEGIN TEST_ID 3';
       STEP 2;
       GOTO MAINLINE;
       LIST 'END TEST_ID 3 FAILED: UNEXPECTED CONTINUATION';
       LIST TITLED WSS;
       QUIT (1);

/*
//*
//INSPLOG  DD SYSOUT=*,DCB=(LRECL=72,RECFM=FB,BLKSIZE=0)
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
