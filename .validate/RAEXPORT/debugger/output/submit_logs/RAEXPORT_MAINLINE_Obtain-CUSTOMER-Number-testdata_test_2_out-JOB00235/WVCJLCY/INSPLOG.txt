      * IBM z/OS Debugger 16.0.2
      * 05/22/2024  4:53:00 PM
      * 5724-T07: Copyright IBM Corp. 1992, 2022
      * The operating system has generated the following message:
      *    EQA2458I SVC Screening is disabled by EQAOPTS. Handling of
      * non-LE events is not available.  Debugging of non-LE programs
      * will be restricted in this z/OS Debugger session.
      *
      * *** Commands file commands follow ***
        DESCRIBE CUS * ;
      * The compile-time data for program RAEXPORT ::> RAEXPORT is
      * Compiler: IBM COBOL  6.4.0        2024/05/22  10:30:20
      *     Its address is X'26B00000' and its length is X'00000F60'
      *     Its linkage is Language Environment FastLink
      *     The statement table has the STMT format.
      *     The program was compiled with the following options:
      *              ADV
      *              NOAWO
      *              CODEPAGE(500)
      *              NOCPYR
      *              DATA(31)
      *              DBCS
      *              NODYNAM
      *              NOFASTSRT
      *              NONUMBER
      *              NUMPROC(NOPFD)
      *              OPTIMIZE(0)
      *              QUA(C)
      *              NOSTGOPT
      *              APOST
      *              RENT
      *              NOSERV
      *              NOSSRANGE
      *              TEST(NOEJPD,SOURCE,NOSEPARATE)
      *              NOTHREAD
      *              TRUNC(STD)
      *              VLR(S)
      *              XMLPARSE(XMLSS)
      *              ZWB
      *              PGMNAME(COMPAT)
      *     The subblocks in this program are nested as follows:
      *        1  RAEXPORT
        LIST %LOAD ;
      * %LOAD = 'RAEXPORT'
        LIST %CU ;
      * %CU = 'RAEXPORT'
        LIST %BLOCK ;
      * %BLOCK = 'RAEXPORT'
        LIST LINE NUMBERS ;
      * Block RAEXPORT ::> RAEXPORT :> RAEXPORT contains the following
      * statements:
      * 338.1         371.1         411.1         486.1
      * 340.1         375.1         413.1         490.1
      * 341.1         376.1         418.1         492.1
      * 342.1         378.1         420.1         494.1
      * 347.1         383.1         421.1         498.1
      * 353.1         384.1         444.1         499.1
      * 354.1         385.1         445.1         501.1
      * 355.1         387.1         446.1         506.1
      * 356.1         388.1         447.1         507.1
      * 360.1         390.1         450.1         508.1
      * 361.1         395.1         473.1         509.1
      * 362.1         396.1         474.1         514.1
      * 365.1         400.1         475.1         515.1
      * 366.1         404.1         476.1         521.1
      * 369.1         409.1         479.1         523.1
      * 370.1         410.1         484.1
        AT EXIT *
          BEGIN ;
            LIST 'END TEST_ID 2 FAILED: UNEXPECTED EXIT' ;
            LIST TITLED WSS ;
            QUIT ( 1 ) ;
          END ;
        01 WCAZVA-COMMAREA PIC X(32768) ;
        AT 404
          BEGIN ;
            LIST 'PATH LINE 404' ;
            LIST 'BEGIN SETUP COMMAREA, IGNORE ERRORS TO END OF SETUP'
       ;
            SET ADDRESS OF DFHCOMMAREA TO ADDRESS OF WCAZVA-COMMAREA ;
            MOVE 32768 TO EIBCALEN OF DFHEIBLK ;
            LIST 'END SETUP COMMAREA' ;
            LIST 'SET RESOURCE INPUTS FOR 404' ;
            MOVE X'60A85C6B5C947D844C93408184E2' TO GENACOUNT ;
            MOVE X'604C4B4C4C944C' TO GENAPOOL ;
            MOVE 0 TO LASTCUSTNUM ;
            MOVE 0 TO WS-RESP ;
            GOTO 409 ;
          END ;
        AT 413
          BEGIN ;
            LIST 'PATH LINE 413' ;
            LIST 'SKIPPING EXECUTION OF 413' ;
            LIST 'BEGIN PROGRAM OUTPUTS FOR 413' ;
            LIST 'BEGIN VAR' ;
            LIST TITLED LASTCUSTNUM ;
            LIST 'END VAR' ;
            LIST 'BEGIN VAR' ;
            LIST TITLED %HEX ( GENAPOOL ) ;
            LIST 'END VAR' ;
            LIST 'BEGIN VAR' ;
            LIST TITLED WS-RESP ;
            LIST 'END VAR' ;
            LIST 'BEGIN VAR' ;
            LIST TITLED %HEX ( GENACOUNT ) ;
            LIST 'END VAR' ;
            LIST 'BEGIN VAR' ;
            LIST TITLED DB2-CUSTOMERNUM-INT OF DB2-OUT-INTEGERS ;
            LIST 'END VAR' ;
            LIST 'END PROGRAM OUTPUTS FOR 413' ;
            LIST 'END TEST_ID 2 AT 413' ;
            QUIT ( 0 ) ;
          END ;
        LIST 'BEGIN TEST_ID 2' ;
      * BEGIN TEST_ID 2
        STEP 2 ;
        GOTO OBTAIN-CUSTOMER-NUMBER OF MAINLINE ;
      * PATH LINE 404
      * BEGIN SETUP COMMAREA, IGNORE ERRORS TO END OF SETUP
      * END SETUP COMMAREA
      * SET RESOURCE INPUTS FOR 404
      * PATH LINE 413
      * SKIPPING EXECUTION OF 413
      * BEGIN PROGRAM OUTPUTS FOR 413
      * BEGIN VAR
      * LASTCUSTNUM = +0000000000
      * END VAR
      * BEGIN VAR
      * GENAPOOL = X'604C4B4C4C944C40'
      * END VAR
      * BEGIN VAR
      * WS-RESP = +0000000000
      * END VAR
      * BEGIN VAR
      * GENACOUNT = X'60A85C6B5C947D844C93408184E24040'
      * END VAR
      * BEGIN VAR
      * DB2-CUSTOMERNUM-INT OF DB2-OUT-INTEGERS = +0000000000
      * END VAR
      * END PROGRAM OUTPUTS FOR 413
      * END TEST_ID 2 AT 413
