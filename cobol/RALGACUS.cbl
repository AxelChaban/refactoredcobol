      ******************************************************************
      * Created: Wed, 22 May 2024 18:45:06 GMT
      * Generated by: IBM watsonx Code Assistant for Z Refactoring
      * Assistant
      * Workbook name: RALGACUS
      * Workbook id: 34847df3-ee57-4aaf-9a76-df86c14b0f9a
      * Project: $clientCOBOL_d37c23ad-5915-400e-b4e7-64699ebade35
      ******************************************************************

       IDENTIFICATION DIVISION.
       PROGRAM-ID. RALGACUS.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY LGPOLICY.
       01  WS-RESPONSE.
           03 WS-RESPONSE-CODE         PIC 9(2).
           03 WS-RESPONSE-MESSAGE      PIC X(78).
       01  CA-ERROR-MSG.
           03 FILLER                   PIC X(9)  VALUE 'COMMAREA='.
           03 CA-DATA                  PIC X(90) VALUE SPACES.
       01  WS-ABSTIME                  PIC S9(8) COMP VALUE +0.
       01  WS-JAVA-VARIABLES.
           03 JAVA                     PIC X(8)  VALUE 'JAVA'.
           03 LGACJV01                 PIC X(8)  VALUE 'LGACJV01'.
           03 WS-STATUS                PIC X(5).
       01  WS-TIME                     PIC X(8)  VALUE SPACES.
       01  WS-DATE                     PIC X(10) VALUE SPACES.
       01  WS-COMMAREA-LENGTHS.
           03 WS-CA-HEADER-LEN         PIC S9(4) COMP VALUE +18.
           03 WS-REQUIRED-CA-LEN       PIC S9(4)      VALUE +0.
       01  ERROR-MSG.
           03 EM-DATE                  PIC X(8)  VALUE SPACES.
           03 FILLER                   PIC X     VALUE SPACES.
           03 EM-TIME                  PIC X(6)  VALUE SPACES.
           03 FILLER                   PIC X(9)  VALUE ' LGACUS01'.
           03 EM-VARIABLE.
             05 FILLER                 PIC X(6)  VALUE ' CNUM='.
             05 EM-CUSNUM              PIC X(10)  VALUE SPACES.
             05 FILLER                 PIC X(6)  VALUE ' PNUM='.
             05 EM-POLNUM              PIC X(10)  VALUE SPACES.
             05 EM-SQLREQ              PIC X(16) VALUE SPACES.
             05 FILLER                 PIC X(9)  VALUE ' SQLCODE='.
             05 EM-SQLRC               PIC +9(5) USAGE DISPLAY.
       01  DFHCOMMAREA-1.
           COPY LGCMAREA.
        01  WS-HEADER.
           03 WS-EYECATCHER            PIC X(16)
                                        VALUE 'LGACUS01------WS'.
           03 WS-TRANSID               PIC X(4).
           03 WS-TERMID                PIC X(4).
           03 WS-TASKNUM               PIC 9(7).
           03 WS-FILLER                PIC X.
           03 WS-ADDR-DFHCOMMAREA      USAGE is POINTER.
           03 WS-CALEN                 PIC S9(4) COMP.

       LINKAGE SECTION.

       PROCEDURE DIVISION.
       MAINLINE SECTION.

      *----------------------------------------------------------------*
      * Common code                                                    *
      *----------------------------------------------------------------*
      * initialize working storage variables
           INITIALIZE WS-HEADER.
      * set up general variable
           MOVE EIBTRNID TO WS-TRANSID.
           MOVE EIBTRMID TO WS-TERMID.
           MOVE EIBTASKN TO WS-TASKNUM.
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      * Process incoming commarea                                      *
      *----------------------------------------------------------------*
      * If NO commarea received issue an ABEND
           IF EIBCALEN IS EQUAL TO ZERO
               MOVE ' NO COMMAREA RECEIVED' TO EM-VARIABLE
               PERFORM WRITE-ERROR-MESSAGE
               EXEC CICS ABEND ABCODE('LGCA') NODUMP END-EXEC
           END-IF

      * initialize commarea return code to zero
           MOVE '00' TO CA-RETURN-CODE
           MOVE '00' TO CA-NUM-POLICIES
           MOVE EIBCALEN TO WS-CALEN.
           SET WS-ADDR-DFHCOMMAREA TO ADDRESS OF DFHCOMMAREA-1.

      * check commarea length
           ADD WS-CA-HEADER-LEN TO WS-REQUIRED-CA-LEN
           ADD WS-CUSTOMER-LEN  TO WS-REQUIRED-CA-LEN

      * if less set error return code and return to caller
           IF EIBCALEN IS LESS THAN WS-REQUIRED-CA-LEN
             MOVE '98' TO CA-RETURN-CODE
             EXEC CICS RETURN END-EXEC
           END-IF

      *----------------------------------------------------------------*
      * Call routine to check first name for numbers                   *
      *----------------------------------------------------------------*
      *************************WCAZ*************************************
           PERFORM CHECK-FIRST.
           If WS-RESPONSE-CODE > 0
             MOVE WS-RESPONSE-CODE TO CA-RETURN-CODE
             DISPLAY WS-RESPONSE-MESSAGE
             EXEC CICS RETURN END-EXEC
           End-if.

           PERFORM FRAUD-CHECK.
           If WS-STATUS NOT = 'valid'
             MOVE '81' TO CA-RETURN-CODE
             EXEC CICS RETURN END-EXEC
           End-if.
      *************************WCAZ*************************************
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      * Call routine to Insert row in DB2 Customer table               *
           PERFORM INSERT-CUSTOMER.
           If CA-RETURN-CODE > 0
             EXEC CICS RETURN END-EXEC
           End-if.

      *----------------------------------------------------------------*
      *
           EXEC CICS RETURN END-EXEC.

       FRAUD-CHECK.
      * Check mobile phone number                                      *
           CALL JAVA   USING     LGACJV01
                                 CA-PHONE-MOBILE
                                 WS-STATUS
                                 CONTENT LENGTH OF CA-PHONE-MOBILE
                                 CONTENT LENGTH OF WS-STATUS.

       CHECK-FIRST.
      * COBOL / Java  switch                                           *
      *    PERFORM CHECK-FIRST-JAVA.
           PERFORM CHECK-FIRST-COBOL.

       CHECK-FIRST-COBOL.
      * Check postcode (COBOL version)                                 *
           MOVE '00' TO WS-RESPONSE-CODE.
           MOVE SPACES TO WS-RESPONSE-MESSAGE.
           IF FUNCTION UPPER-CASE (CA-POSTCODE(1:2)) = 'GB'

               CONTINUE
           ELSE IF FUNCTION UPPER-CASE (CA-POSTCODE(1:2)) = 'US'

               CONTINUE
           ELSE IF FUNCTION UPPER-CASE (CA-POSTCODE(1:2)) = 'UK'

               CONTINUE
           ELSE IF FUNCTION UPPER-CASE (CA-POSTCODE(1:2)) = 'DN'

               CONTINUE
           ELSE
               MOVE '82' TO WS-RESPONSE-CODE
               STRING 'Invalid postcode: ' CA-POSTCODE
                DELIMITED BY SIZE INTO WS-RESPONSE-MESSAGE
           END-IF.

       INSERT-CUSTOMER.
           IF WS-STATUS = 'valid'
               EXEC CICS LINK PROGRAM('LGACDB01')
                   COMMAREA(DFHCOMMAREA-1)
                   LENGTH(32500)
               END-EXEC
           ELSE IF WS-STATUS = 'error'
               EXEC CICS ABEND ABCODE('CUSE') NODUMP END-EXEC
           END-IF.
           EXIT.

       WRITE-ERROR-MESSAGE.
      * Save SQLCODE in message
      * Obtain and format current time and date
           EXEC CICS ASKTIME ABSTIME(WS-ABSTIME)
           END-EXEC
           EXEC CICS FORMATTIME ABSTIME(WS-ABSTIME)
                     MMDDYYYY(WS-DATE)
                     TIME(WS-TIME)
           END-EXEC
           MOVE WS-DATE TO EM-DATE
           MOVE WS-TIME TO EM-TIME
      * Write output message to TDQ
           EXEC CICS LINK PROGRAM('LGSTSQ')
                     COMMAREA(ERROR-MSG)
                     LENGTH(LENGTH OF ERROR-MSG)
           END-EXEC.
      * Write 90 bytes or as much as we have of commarea to TDQ
           IF EIBCALEN > 0 THEN
             IF EIBCALEN < 91 THEN
               MOVE DFHCOMMAREA-1(1:EIBCALEN) TO CA-DATA
               EXEC CICS LINK PROGRAM('LGSTSQ')
                         COMMAREA(CA-ERROR-MSG)
                         LENGTH(LENGTH OF CA-ERROR-MSG)
               END-EXEC
             ELSE
               MOVE DFHCOMMAREA-1(1:90) TO CA-DATA
               EXEC CICS LINK PROGRAM('LGSTSQ')
                         COMMAREA(CA-ERROR-MSG)
                         LENGTH(LENGTH OF CA-ERROR-MSG)
               END-EXEC
             END-IF
           END-IF.
           EXIT.

           EXIT PROGRAM.
